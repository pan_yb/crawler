import struct
import datetime
import functools
#import crawler.core.rbtree as rbtree
from pool import ConnectionPool
import re
import sys
import crawler.net.ioloop as ioloop

"""
def SDBMHash(str):
    hash = 0
    i = 0
    while i< len(str):
        hash = ord(str[i]) + (hash << 6) + (hash <<16) - hash
        i += 1
    return hash & 0x7fffffff    
"""



def SDBMHash(str):
    hash = 0
    i = 0
    while i< len(str):
        hash = ord(str[i]) + (hash << 6) + (hash <<16) - hash
        i += 1
    return hash & 0xfff  
    
    
class HostInfo(object):
    def __init__(self , hostname, ipaddr, duetime):
        self.hostname = hostname
        self.ipaddr = ipaddr
        self.duetime = duetime
        
        


class DnsOper(object):
    id = 3 #random number
    def __init__(self):
        self.sign = 0x0100  #standard query
        self.requestsnum= 1
        self.resnum = 0
        self.authresnum = 0
        self.extraresnum = 0
        self.requests =''
        self.response = ''
        self.authres = ''
        self.extrares = ''
        
        self.waittime = 5 # 5s
        
        self.ipre_str = r'((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)'
        
        self.urlname_ipaddr = {}
        self.urlname_callback = {}
        self.urlname_timeout = {}
        
        self.__ioloop = ioloop.IOLoop.instance()
        
        self.pool = ConnectionPool()
        self.hashslots = {} #hashcode :(url, duedate),...

        
        
        
    def get_ipaddr(self, hostname, callback):
        hostname = hostname.strip()
        ipre = re.compile(self.ipre_str)
        if ipre.match(hostname):
            return hostname
        hash = SDBMHash(hostname)
        if self.hashslots.has_key(hash):
            hostinfo = self.hashslots[hash]
            if hostinfo[0] == hostname and hostinfo[2] < self.__ioloop.get_currenttime():
                return hostinfo[1]
                
        if not self.urlname_callback.has_key(hostname):
            self.urlname_callback[hostname] = [callback]
            try:
                self.send_request(hostname)
            except:
                callback(None) 
                #self.urlname_ipaddr.pop(hostname)
                self.urlname_callback.pop(hostname)
                #print sys.info()
                raise
        else:     
            self.urlname_callback[hostname].append(callback)
        return None
    
        
    def send_request(self,hostname):
        hostnamelist = hostname.split('.')
        requesthost = ''
        for value in hostnamelist:
            requesthost += chr(len(value))+value
        requesthost += chr(0)
        
        request = struct.pack('!hhhhhh%dshh' % len(requesthost),DnsOper.id,self.sign,self.requestsnum, self.resnum ,
                              self.authresnum ,self.extraresnum , requesthost, 1, 1)
        
        DnsOper.id += 1
        DnsOper.id &= 0xfffffff
        
        connection = self.pool.get_free_connection()
        self.add_process_timer(hostname, self.waittime, self.timeout_func, hostname, connection)
        wraper = functools.partial(self.__parse_dnspacket, hostname)
        #try:
        connection.send_request(request, wraper)
        #except IOError:
        #    raise

  
  
  
  
    def timeout_func(self, urlname, connection):
        """
        timeout function that will be called if the process of getting the
        ipaddr  times out
        """
        if self.urlname_timeout[urlname][0] > 3:
            for eachfunc in self.urlname_callback[urlname]:
                eachfunc(None)
            self.urlname_callback.pop(urlname)
            self.urlname_timeout.pop(urlname)
            connection.close()
        else:
            self.add_process_timer(urlname, self.waittime, self.timeout_func, urlname, connection)
            
             
  
    def add_process_timer(self, urlname, delta_seconds, callback, *args , **kwargs):
        wrapper = functools.partial(callback, *args, **kwargs)
        timeout = self.__ioloop.add_timeout(datetime.timedelta(seconds = delta_seconds) , wrapper)

        if(self.urlname_timeout.has_key(urlname)):
            self.urlname_timeout[urlname][0] += 1
            self.urlname_timeout[urlname][1] = timeout
        else:        
            self.urlname_timeout[urlname] = [1, timeout] #most try times 
        
        
        
    def remove_timer(self, urlname):
        self.__ioloop.remove_timeout(self.urlname_timeout[urlname][1]) 
        self.urlname_timeout.pop(urlname)
          
              
  
    def __parse_dnspacket(self ,hostname, response):
        error_flag = False
        id,flags,qdcount,ancount,nscount,arcount = struct.unpack("!hhhhhh" , response[:12])
        if flags & 0x8000 == 0 or flags & 0x80 == 0 or flags & 0x100 == 0 :
            error_flag = True
        answer = response[12:]   
        for i in range(0,qdcount):
            j = 0
            while(ord(answer[j])):
                j += ord(answer[j]) + 1
            answer = answer[j+5:]  
        urlname_temp = []    
        for i in range(0,ancount):
            error_flag = False
            j = k = 0
            urlname = ipaddr = ''
            if(ord(answer[j]) & 0xc0):
                k = (((ord(answer[j]) & (~0xc0)))<<8 )| ord(answer[j+1])
                while(ord(response[k])):
                    urlname += response[k+1:k+1+ord(response[k])] + '.'
                    k += ord(response[k]) + 1
                    if ord(response[k]) & 0xc0:
                        urlname += 'com'
                        #error_flag = True
                        break
                urlname = urlname[:-1]
                answer = answer[2:]
            else:
                while(ord(answer[j])):
                    urlname += answer[j+1:j+1+ord(answer[j])] + '.'
                    j += ord(answer[j]) + 1
                urlname = urlname[:-1]    
                answer = answer[j + 1:] 
            #urlname_temp.append(urlname)
            mes_type,mes_class,mes_ttl, mes_datalen = struct.unpack("!hhih",answer[:10])
            if mes_type != 0x1 or mes_class != 0x1:
                error_flag = True
            if mes_datalen == 4:
                ipaddr = str(ord(answer[10])) + '.' + str(ord(answer[11])) + '.' + str(ord(answer[12])) +'.'+str(ord(answer[13]))
            else:
                error_flag = True
            answer = answer[10 + mes_datalen:] 
            if error_flag:
                self.urlname_ipaddr[hostname] = None
            else:
                self.urlname_ipaddr[hostname] = ipaddr
                self.hashslots[SDBMHash(hostname)] = (hostname, self.urlname_ipaddr[hostname], self.__ioloop.get_currenttime() + mes_ttl)
                """add to the red-black tree"""
                
                #self.dnsrbtree.insert(SDBMHash(hostname), HostInfo(hostname, self.urlname_ipaddr[hostname],mes_ttl ))
                #self.add_rbtree_timer(i, mes_ttl, self.rbtree.delete_node, SDBMHash(i))                
                break         
        #for i in urlname_temp:
        
        for eachfunc in self.urlname_callback[hostname]:
            print ipaddr
            print self.urlname_ipaddr[hostname]
            eachfunc(self.urlname_ipaddr[hostname]) 
        self.urlname_ipaddr.pop(hostname)
        self.urlname_callback.pop(hostname)
        #all urlname share one timer, remove one is fine
        self.remove_timer(hostname) # remove the timeout handler 
 
      
        
        
           
 