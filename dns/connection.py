import crawler.net.iostream as iostream
import socket

class Connection(object):
    def __init__(self,pool, ipaddr , port):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
        self.__stream = iostream.IOStream(s)
        try:
            s.connect((ipaddr, port))
        except:
            pass
        self.__pool = pool
        
    def close(self):
        self.__stream.close()
        
    def release(self):
        self.__pool.free_connection(self)
              
    def timeout(self):
        pass         

        
    def send_request(self , request, callback):
        def wrapper(response):
            callback(response)
            self.release()
        self.__stream.write(request)
        self.__stream.read_bytes(1024, None, streaming_callback = wrapper)
                 
                 
                 
                 
   
                                       