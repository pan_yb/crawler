from connection import  Connection


class ConnectionPool(object):
    def __new__(cls, *args, **kw):
        if not hasattr(cls, '_instance'):
            orig = super(ConnectionPool, cls)
            cls._instance = orig.__new__(cls, *args, **kw)
            cls._instance.__initialize()
        return cls._instance
    
    def __init__(self):
        pass
    
    def __initialize(self):
        self.__connections = []
        self.__dnsipaddr = ["192.168.1.1"]
        self.__whichdns = 0
        
    
    def get_free_connection(self):
        try: # first try to get it from the idle cache
            connection = self.__connections.pop(0)
        except IndexError: # else get a fresh connection
            connection = Connection(self, self.__dnsipaddr[self.__whichdns], 53)
            self.__whichdns += 1
            if self.__whichdns == len(self.__dnsipaddr):
                self.__whichdns = 0
        return connection
            
                    
    def free_connection(self, connection):
        self.__connections.append(connection)
        
        
        
    def __del__(self):
        for connection in self.__connections:
            connection.close()
    