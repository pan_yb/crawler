import basestrategy
import re
#import crawler.http.message as message

class SinaBlog(basestrategy.BaseStrategy):
    WEBSITENO = 1
    def __init__(self):
        super(SinaBlog, self).__init__()
        self.type_func = (self.parse_type1,self.parse_type2)
        
        
    def parse_type1(self, htmlcontent):
        #print htmlcontent
        htmlcontent = htmlcontent.decode('gb2312','ignore')
        re_str = re.compile(ur'"title"\s*:\s*"([\u4e00-\u9fa5\u3000-\u303f\ufb00-\ufffd]+)"\s*,\s*"url"\s*:\s*"(http://[\w\./\?\=]+)"')
        res_list = re_str.findall(htmlcontent)
        response_msg = []
        for res in res_list:
            response_msg.append((SinaBlog.WEBSITENO, 2,str(res[1])))
            print res[0], res[1]
        return response_msg
        
    def parse_type2(self, htmlcontent):    
        #htmlcontent = htmlcontent.decode('gb2312','ignore')
        super(SinaBlog,self).extract_maintext(htmlcontent)
        response_msg = []
        return response_msg
        
    def do_parse(self, typeno, msg):       
        """
        pattern : urlindex,  [(websiteno1, typeno1, url1),...]
        """ 
        return (self.type_func[typeno - 1 ])(msg)
        
        
    
    def __del__(self):
        super(SinaBlog, self).__del__()
    
    def save_general(self, htmlcontent):
        pass