from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol

from hbase import Hbase
from hbase.ttypes import *


class BaseStrategy(object):
    def __init__(self):
        self.__initialize()
        pass
    
    def __initialize(self):
        self.__tablename = 'crawler'
        self.__transport = TSocket.TSocket('localhost', 9090)
        self.__transport = TTransport.TBufferedTransport(self.__transport)
        self.__protocol = TBinaryProtocol.TBinaryProtocol(self.__transport)
        self.__client = Hbase.Client(self.__protocol)
        self.__transport.open()
     
    def __del__(self):
        self.__close()
        
    def __close(self):
        self.__transport.close()    
        
    def create_table(self):
        html = ColumDescriptor(name = 'html')  
        self.__client.createTable(self.__tablename, [html])
    
    def put(self, msg):
        url = Mutation(column = 'html:url', value = msg.url) 
        htmlcontent = Mutation(column = 'html:htmlcontent', value = msg.htmlcontent) 
        self.__client.mutateRow(self.__tablename, "", [url, htmlcontent], None)
           
    def do_parse(self, typeno, msg):
        self.put(msg)

class Message(object):
    def __init__(self, url, htmlcontent):
        self.url = url
        self.htmlcontent = htmlcontent


if __name__ == '__main__':
       basestrategy = BaseStrategy()
       msg = Message( "www.163.com", "fdsffffdsf")
       basestrategy.do_parse(0, msg)      