import basestrategy
import sinablog

"""
all the html parser strategies listed here
pattern : 'clsname': (cls, (args)s)
"""
allstrategy_info = { 0: ['basestrategy' , basestrategy.BaseStrategy, None],
                     1: ['sinablog' , sinablog.SinaBlog, None]}