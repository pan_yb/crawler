from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol

from hbase import Hbase
from hbase.ttypes import *

from readability.readability import Document


class BaseStrategy(object):
    def __init__(self):
        #self.__initialize()
        pass
    
    def __initialize(self):
        self.__tablename = 'crawler'
        try:
            self.__transport = TSocket.TSocket('localhost', 9090)
            self.__transport = TTransport.TBufferedTransport(self.__transport)
            self.__protocol = TBinaryProtocol.TBinaryProtocol(self.__transport)
            self.__client = Hbase.Client(self.__protocol)
            self.__transport.open()
        except:
            print 'basestrategy: count not connect to tsockt'
     
    def __del__(self):
        try:
            self.__close()
        except:
            pass
        
    def __close(self):
        self.__transport.close()    
        
    def create_table(self):
        html = ColumDescriptor(name = 'html')  
        self.__client.createTable(self.__tablename, [html])
    
    def put(self, msg):
        print 'in parser'
        print msg
        """
        url = Mutation(column = 'html:url', value = msg.url) 
        htmlcontent = Mutation(column = 'html:htmlcontent', value = msg.htmlcontent) 
        self.__client.mutateRow(self.__tablename, "", [url, htmlcontent], None)
        """
    
    def extract_maintext(self, htmlcontent):
        readable_article = Document(htmlcontent).summary()
        readable_title = Document(htmlcontent).short_title()
        print readable_title
        
     
           
    def do_parse(self, typeno, msg):
        """
        pattern : urlindex,  [(websiteno1, typeno1, url1),...]
        """
        title, article = attract_maintext(msg)
        print title, article
        #self.put(msg)
        return None
"""
class Message(object):
    def __init__(self, url, htmlcontent):
        self.url = url
        self.htmlcontent = htmlcontent

"""
if __name__ == '__main__':
       basestrategy = BaseStrategy()
       msg = Message( "www.163.com", "fdsffffdsf")
       basestrategy.do_parse(0, msg)      