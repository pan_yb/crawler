import crawler.core.lru
class SingletonManager(object):
    """
    this class is used to manage the singletons ,
    for so many singletons that,it may run out of
    system memory, we use lru to solve this .
    """
    def __init__(self, totalnum):
        """
        totalnum is the total slot count in lru 
        """
        self.__singletonpool = lru.Lru(totalnum)
        
    def register(self, key, singleton):
        """
        invoker call this function to register a singleton,
        add it to  the pool
        """
        self.__singletonpool[key]
        
    def find(self, key):
        return self.__singletonpool[key]
    
    def destroy(self):
        """
        destroy all the singletons left in the lru
        """
        del self.__singletonpool
           