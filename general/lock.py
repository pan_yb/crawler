import threading
class Lock(object):
    def __init__(self):
        pass
    def acquire(self, shouldblock = False):
        raise NotImplementedError()
    def release(self):
        raise NotImplementedError()
    
class GeneralLock(Lock):
    def __init__(self):
        self.__lock = threading.Lock()
        
    def acquire(self, shouldblock):
        self.__lock.acquire(shouldblock)
        
    def release(self):
        self.__lock.release()


class NoLock(Lock):
    def __init(self):
        pass
    def acquire(self, shouldblock = False):
        return True
    def release(self):
        pass














