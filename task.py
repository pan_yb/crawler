import logging
import functools
import datetime
import os
import sys
import crawler.http.urlsoper as urlsoper
import crawler.dns.dnsoper as dnsoper
import crawler.http.htmlgetter as htmlgetter
import crawler.general.lock as lock
import crawler.http.message as message
import crawler.net.ioloop as ioloop
import crawler.net.iostream as iostream




class MainTask(object):
    RUNNING, PAUSE, STOP = range(3)
    def __init__(self):
        self.__urlsoper = urlsoper.UrlsOper('mongodb')
        self.__dnsoper = dnsoper.DnsOper()
        self.__runflag = MainTask.STOP
        logging.basicConfig(filename = os.path.join(os.getcwd(), 'log.txt'), \
    level = logging.WARN, filemode = 'w', format = '%(asctime)s - %(levelname)s: %(message)s')  
        self.logging = self.__logging = logging
        self.__htmlgetterpool = htmlgetter.HtmlGetterPool(self.__logging)
        self.__htmlparser_proxy = htmlgetter.HtmlParserFactory_Server(self.__logging).get_htmlparser(1)#any number
        self.__ioloop = ioloop.IOLoop.instance()
        self.__controller_ip = "127.0.0.1"
        self.__controller_port = 45678
        
        self.__urlswindowlen = 30
        self.__urlswindowmax = 1000
        #like tcp send window
        self.__urlinhandlinginfo = []
        #the first element sequence number 
        self.__urlinhandlinginfo_startindex = 0
        self.__urlinhandlinginfo_maxindex = 888
        #timer for urlinhandling  pattern: [(startindex, endindex(not include), timer),...]
        self.__urlinhandlinginfo_timer = [] 

        # to see if there is any response
        self.__response_timer = None
        
        #self.__htmlparser = parsehttp.htmlparser()
        #hbase
        

        
    
    """
    urlinfo = (url, websiteno, typeno)
    """    
    def put_urlsready(self, urlsinfofromdb):
        if not len(urlsinfofromdb):
            return 
        seqno = self.__urlinhandlinginfo_startindex + len(self.__urlinhandlinginfo)
        for urlinfo in urlsinfofromdb:
            url = urlinfo[0]
            websiteno = urlinfo[1]
            typeno = urlinfo[2]
            #we first put this url in the urlinhandling
            self.__urlinhandlinginfo.append(urlinfo)
            retstart = url.find('://')
            retstart += 3
            if retstart == -1:
                retstart = 0
            retend1 = url.find(':', retstart)
            retend2 = url.find('/', retstart)
            if retend1 == -1:
               retend = retend2
               if retend2 == -1:
                   retend = None
            else:
                retend = retend1
            hostname = url[retstart : retend]
            if retend2 == -1:
                lefturl = '/'
            else:
                lefturl = url[retend2 : ]
            wrap_callback = functools.partial(self.get_htmlcontent, seqno, hostname, lefturl, websiteno, typeno)
            seqno += 1
            ipaddr = self.__dnsoper.get_ipaddr(hostname, wrap_callback)
            if ipaddr != None:
                """get the ip directly"""
                wrap_callback(url[retend:], ipaddr)
        #add timer
        appendendindex = self.__urlinhandlinginfo_startindex + len(self.__urlinhandlinginfo)
        wrapper = functools.partial(self.__timer_callback, appendendindex)
        timeout = self.__ioloop.add_timeout(datetime.timedelta(seconds = 20) , wrapper)
        self.__urlinhandlinginfo_timer.append((appendendindex,timeout))
                

        
    def __timer_callback(self, endindex):
        """
        remove the timer
        remove overdue url element in urlinhandling
        logging error msg
        """
        count = 0;
        for timerinfo in self.__urlinhandlinginfo_timer:
            if timerinfo[0] <= endindex:
                count += 1
        if not count:
            return
        self.__urlinhandlinginfo_timer = self.__urlinhandlinginfo_timer[count:] 
        if endindex <= self.__urlinhandlinginfo_startindex:
            print 'should not be here'
            return
        failedurlinfos = self.__urlinhandlinginfo[0: ( endindex - self.__urlinhandlinginfo_startindex)]
        failed_seqno = self.__urlinhandlinginfo_startindex
        for failedurlinfo in failedurlinfos:
            if failedurlinfo:
                print failedurlinfo
                self.__logging.warning("task: timeout , seqno is %d, url is %s" %(failed_seqno, failedurlinfo[0]))
            failed_seqno += 1
        self.__urlinhandlinginfo = self.__urlinhandlinginfo[( endindex - self.__urlinhandlinginfo_startindex ) :]
        self.__urlinhandlinginfo_startindex = endindex
        if self.__urlinhandlinginfo_startindex > self.__urlinhandlinginfo_maxindex:
            self.__urlinhandlinginfo_startindex = 0


            
    def get_htmlcontent(self, seqno, hostname, lefturl, websiteno, typeno, ipaddr):
        if not ipaddr:
            print 'hostname %s error'  %hostname
        html_getter = htmlgetter.HtmlGetterPool(None).get_free_htmlgetter(websiteno)
        html_getter.connect(seqno, hostname, ipaddr, 80, lefturl, websiteno, typeno)    
  
    """          
    def __handle_url(self, urlwithoutip, websiteno, typeno, ipaddr):
        html_getter = htmlgetter.HtmlGetterPool(self.__logging).get_free_htmlgetter(ipaddr)
        html_getter.connect(self, ipaddr, port, urlwithoutip, websiteno, typeno)
    """
        


    """
    adjust produce speed
    """
    def __adjust_urlwindow(self, speedflag):
        """
        speedflag == 0 : no adjustment
        speedflag == 1: +4
        speedflag ==-1: -4
        """
        if -1 == speedflag: 
            self.__urlswindowlen -= 4
            self.__urlswindowlen = max(self.__urlswindowlen, 0)
        elif 1 == speedflag:
            self.__urlswindowlen += 4 
            self.__urlswindowlen = min(self.__urlswindowlen, self.__urlswindowmax)
        
        
                      
    def handler_control_cmd(self, cmdname):
        """
        receive control cmd from the control process, and handle it
        """
        print cmdname.strip()
        try:   
            cmd = control_cmd[cmdname.strip()]
        #call this command with this class instance
            cmd(self)
        except :
            info=sys.exc_info()  
            print info #info[0],":",info[1] 
         #   """@@@@@@@@@@@@@@@@@@@@@@@@@@"""
        #    pass
         
    def control_start(self):
        self.__runflag = MainTask.RUNNING
        self.__urlsoper.geturls(self.__urlswindowlen , self.put_urlsready) 
        self.run()
        
    
    def control_stop(self):
        self.__runflag = MainTask.STOP    
        self.run()
     
    def control_pause(self):
        self.__runflag = MainTask.PAUSE     
        self.run()   
     
    def initialize(self):
        self.stream = iostream.PipeIOStream(0)
        self.stream.read_bytes(30, None, self.handler_control_cmd)
        """
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
        self.__controller_stream = tornado.iostream.IOStream(s)
        #stream.connect(self.__controller_ip, self.__controller_port)
        #suppose len(cmd) should be less than 1024
        self.__controller_stream.read_bytes(1024,self.handler_control_cmd)
        """
        #event loop start...
        self.__ioloop.start()
     
    
    def handle_responses(self):
        print '...startindex is ', self.__urlinhandlinginfo_startindex 
        speedflag = self.__htmlparser_proxy.should_adjustproducer()
        self.__adjust_urlwindow(speedflag)
        #get the response from the workers
        responses = self.__htmlparser_proxy.get_responses() 
        self.__handle_responses(responses)
                # to see if there is any response
        self.__response_timer = self.__ioloop.add_timeout(datetime.timedelta(seconds = 1) , self.handle_responses)
            
    def run(self):       
        """
        get the html and let the workers parse it 
        the worker put the parsed data into db directly
        and send back the urls 
        """
        
        if self.__runflag == MainTask.RUNNING:
            self.handle_responses()
        elif self.__runflag == MainTask.STOP:
            #record url state
            self.__urlsoper.saveurls(self.__urlinhandlinginfo, callback = None)
            for urlinfo in self.__urlinhandlinginfo:
                self.__logging.warning("url %s canceled. \n" %urlinfo[2])
            self.__clear_timerifneed(len(self.__urlinhandlinginfo)+ self.__urlinhandlinginfo_startindex)
            self.__ioloop.remove_timeout(self.__response_timer)
            self.__ioloop.stop()
        else : #pause
            pass
            
  
    def log_saveurlerror(self, result, error):
        if error:
            self.logging.warning('save error happend %s', error)
  
    def __handle_responses(self,responses):
        """
        response pattern:(urlindex, urlsinfo)
        urlsinfo pattern:[(websiteno, typeno, urlcontent) , ...]
        """
        if not len(responses):
            return          
        response_urlindexs = []
        responsemsgs = message.ResponseMessage()
        for response in responses:
            urlindex , urlsinfo = responsemsgs.unpack(response)
            print 'ffresponse len is %d, %d', len(response), urlindex
            response_urlindexs.append(urlindex)
            #save the urls into dbs        
            if len(urlsinfo):
                print urlsinfo
                print len(urlsinfo)
                #self.__urlsoper.saveurls(urlsinfo, callback = self.log_saveurlerror)
        self.__clear_urlrequest( response_urlindexs )
            
        
    
    def __clear_timerifneed(self, resurl_indexend):
        """
        timerinfo = (urlendindex, timer)
        """
        count = 0
        for timerinfo in self.__urlinhandlinginfo_timer:
            if  timerinfo[0] <= resurl_indexend:
                #cancel this timer
                count += 1
                self.__ioloop.remove_timeout(timerinfo[1])
            else:
                break
        self.__urlinhandlinginfo_timer = self.__urlinhandlinginfo_timer[count :]
            
    def __clear_urlrequest(self, response_urlindexs ):
        print 'resposne urlindex is ' , response_urlindexs
        for urlindex in response_urlindexs:
            #late response , ignore
            if urlindex < self.__urlinhandlinginfo_startindex:
                continue
            #set the corresponding url none
            self.__urlinhandlinginfo[urlindex - self.__urlinhandlinginfo_startindex] = None
        #to see if we can move the startindex forward
        count_add =0
        for urlelement in self.__urlinhandlinginfo:
            if not urlelement :
                count_add += 1
            else:
                break
        if  count_add:
            self.__urlinhandlinginfo_startindex += count_add  
            self.__urlinhandlinginfo =self.__urlinhandlinginfo[ count_add :]
            self.__clear_timerifneed(self.__urlinhandlinginfo_startindex ) 
        #if over threadhold ,get new urls
        if (len(self.__urlinhandlinginfo)<<2) < self.__urlswindowlen * 3: 
            if self.__urlinhandlinginfo_startindex > self.__urlinhandlinginfo_maxindex:
                self.__urlinhandlinginfo_startindex = 0
            count_add = self.__urlswindowlen - len(self.__urlinhandlinginfo)   
            if self.__runflag == MainTask.RUNNING:
                self.__urlsoper.geturls(count_add, self.put_urlsready) 

                
                         
  

control_cmd = {'start': MainTask.control_start,
               'stop': MainTask.control_stop,
               'pause': MainTask.control_pause
               }


if __name__ == '__main__':
    maintask = MainTask()
    maintask.initialize()
        
             
        
                    
            