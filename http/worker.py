import multiprocessing
import message
import crawler.strategy.strategy_map as strategy_map
import crawler.strategy.basestrategy 
import time
class ParserWorker(object):
    def __init__(self,  workerid, responseid, workerlock, condition):
        """semaphore is used to communicate with main process
        workerid is the id of this parserworker"""
        self.__workermsgoper = message.WorkerMsgOper(workerid,  responseid, workerlock, message.SERV_WORKER_CHAN_LEN )
        self.__condition = condition
        self.__runflag = True
        self.__msgs = None
        self.__process = multiprocessing.Process(target = self.task)
        self.__process.start()
        
    def task(self):    
        maxcount = 20
        request_msg = message.RequestMessage()
        response_msg = message.ResponseMessage()
        #baseparser = basestrategy.BaseStrategy()
        while self.__runflag:
            """
            parse the html content gotten from the workerchannel in workermsgoper
            """            
            print 'worker task'
            self.__condition.acquire()
            self.__condition.wait()
            htmlmsgs = self.__workermsgoper.get_msgs(maxcount)
            self.__condition.release()
            for htmlmsg in htmlmsgs:
                seqno, websiteno, typeno, htmlcontent =request_msg.unpack(htmlmsg)
                print 'seqno is ', seqno, len(htmlmsg)
                #if seqno >500:
                    #continue
                #time.sleep(1)
                #continue
                """parsing
                pattern : urlindex,  [(websiteno1, typeno1, url1),...]
                  """

                #urlsinfo = baseparser.do_parse(htmlcontent)
                #response_msg.urlindex = seqno
                #response_msg.urlsinfo = urlsinfo   
                webparser_cls = strategy_map.allstrategy_info[websiteno][1]
                if not strategy_map.allstrategy_info[websiteno][2] :
                    strategy_map.allstrategy_info[websiteno][2] = webparser_impl  = webparser_cls()
                response_msg.urlindex = seqno 
                response_msg.urlsinfo = webparser_impl.do_parse(typeno, htmlcontent)
                """
                try:
                    webparser_cls = strategy_map.allstrategy_info[websiteno][1]
                    if not strategy_map.allstrategy_info[websiteno][2] :
                        strategy_map.allstrategy_info[websiteno][2] = webparser_impl  = webparser_cls()
                    response_msg.urlindex = seqno 
                    response_msg.urlsinfo = webparser_impl.do_parse(typeno, htmlcontent)
                except  :
                    print 'worker.py: parsing error'
                    pass
                """
                    
                
                #print htmlcontent
                print response_msg.pack()
                self.__workermsgoper.put_response((response_msg.pack(),))
                
        self.__workermsgoper.destroy()
                
    def stop(self):
        self.__runflag = False    
        self.__process.join()
        