import struct
import mmap
import multiprocessing
import worker 
import os
import ctypes


class ResponseMessage(object):
    """
    pattern : urlindex,  [(websiteno1, typeno1, url1),...]
    packedstr : urlindex, urlinfonum, websiteno1, typeno1, len1, url1, website2...
    """
    def __init__(self, urlindex = 0  , urlsinfo = [], packstr = '' ):
        self.urlindex = urlindex
        self.urlsinfo = urlsinfo
        self.packstr = packstr
     
    def get_length(self):
        return  len(self.packstr)   
    
    def pack(self):
        urlsinfo_packed = ''
        for urlinfo in self.urlsinfo:
            #print urlinfo
            urlsinfo_packed += struct.pack('iii%ds' %len(urlinfo[2]), urlinfo[0], urlinfo[1], len(urlinfo[2]), urlinfo[2] )            
        self.packstr = struct.pack("ii%ds" %len(urlsinfo_packed), self.urlindex, len(self.urlsinfo), urlsinfo_packed)
        return self.packstr
        
    def unpack(self, packstr):
        urlsinfo = []
        self.packstr = packstr
        self.urlindex, urlsinfonum, urlsinfo_packed = struct.unpack("ii%ds" %(len(self.packstr)-4 -4), self.packstr)
        while urlsinfonum:
            #print 'len is %d', len(urlsinfo_packed)
            websiteno , typeno, urllen = struct.unpack("iii", urlsinfo_packed[:12])
            urlcontent, = struct.unpack("%ds" %urllen, urlsinfo_packed[12: urllen + 12])
            urlsinfo_packed = urlsinfo_packed[(12 + urllen) :]
            urlsinfo.append((websiteno, typeno, urlcontent))
            urlsinfonum -= 1
        return (self.urlindex, urlsinfo)


class RequestMessage(object):
    def __init__(self, seqno = 0 , websiteno = 0 , typeno = 0 , htmlcontent = "", packstr= ""):
        self.__seqno = seqno
        self.__websiteno = websiteno
        self.__typeno = typeno
        self.__htmlcontent = htmlcontent
        self.__packstr = packstr

     
    def get_length(self):
        return  len(self.__packstr)   
    
    def pack(self):
        self.__packstr = struct.pack("iii%ds" %len(self.__htmlcontent), self.__seqno, self.__websiteno, self.__typeno, self.__htmlcontent)
        return self.__packstr
        
    def unpack(self, packstr):
        self.__packstr = packstr
        htmlcontentlen = len(self.__packstr) - 4 -4 -4
        self.__seqno, self.__websiteno, self.__typeno, self.__htmlcontent = struct.unpack("iii%ds" %htmlcontentlen , self.__packstr)
        return (self.__seqno, self.__websiteno, self.__typeno, self.__htmlcontent)        
 
SERV_WORKER_CHAN_LEN = 5*1024*1024         
class ServerMsgOper():
    curid = 0
    responseid = 777 #any number larger thar curid
    def __init__(self, logging , channelspace = SERV_WORKER_CHAN_LEN , maxworker = 20):
        self.__channelspace = channelspace
        self.__roundrobin_index = 0
        self.__atomlib = ctypes.cdll.LoadLibrary('/home/pan/workspace/crawler/core/atomoper.so')
        self.__response_channel = MessageChannel(self.__atomlib, isproducer = False, channelid = ServerMsgOper.responseid, length = self.__channelspace)
        self.__workerlock = multiprocessing.Lock()      
        self.__workers = []
        self.__worker_channels = []
        self.__worker_channels_condition = []
        self.__maxworker = maxworker
        self.spawn_workers(4)
        self.__logging = logging 
        self.__adjustproducer = 0  # 0 no need adjust ,-1 slowdown; 1 faster
    
    def spawn_workers(self, count):
        """ initialize the works"""
        for workercnt in range(count):    
            condition = multiprocessing.Condition()            
            self.__workers.append(worker.ParserWorker( ServerMsgOper.curid,  ServerMsgOper.responseid, self.__workerlock, condition))
            self.__worker_channels.append(MessageChannel(self.__atomlib, isproducer = True, channelid = ServerMsgOper.curid, length = self.__channelspace))
            self.__worker_channels_condition.append(condition)
            ServerMsgOper.curid += 1
    
    
    def put_msg_to_worker(self, msg):
        """poll to see if any work channel is available,
        if not,  spawn some , if the worker count is already too many, give up"""    
        trytimes = 2
        round_robin = range(len(self.__workers))[self.__roundrobin_index:] + range((len(self.__workers)))[:self.__roundrobin_index]
        for j in range(trytimes):
            for i in round_robin:
                if self.__worker_channels[i].get_leftspace() >= len(msg) :
                    self.__worker_channels_condition[i].acquire()
                    self.__worker_channels[i].put_msgs((msg,))
                    self.__worker_channels_condition[i].notify()
                    self.__worker_channels_condition[i].release()
                    self.__roundrobin_index = i + 1
                    if j == 0:
                        #if is the first trial, set faster
                        self.__adjustproducer = 1 
                    return True
        if len(self.__workers) < self.__maxworker:
            self.spawn_workers(5)
            #if len(self.__workers) > 2/3 maxworkers   , set adjustproducer flag
            if len(self.__workers) * 3 /2 > self.__maxworker :
                self.__adjustproducer = -1
            for i in round_robin:
                if self.__worker_channels[i].get_leftspace() >= len(msg) :
                    self.__worker_channels_condition[i].acquire()
                    self.__worker_channels[i].put_msgs((msg,))
                    self.__worker_channels_condition[i].notify()
                    self.__worker_channels_condition[i].release()
                    self.__roundrobin_index += 1
                    return True
                else:
                    errormsg = 'ServerMsgOper too less workers' 
                    print errormsg
                    self.__logging.error(errormsg)
                    return False
        else:
            errormsg = 'ServerMsgOper too less workers' 
            print errosmsg
            self.__logging.error(errormsg)
            return False
            
    def get_responses(self):
        """
        responses = [response1,....,response5...], len <=1024
        """
        responses = self.__response_channel.get_msgs(1024)
        return responses
    
    def stop_worker(self):
        for worker in self.__workers:
            worker.stop()
        for worker_channel in self.__worker_channels:
            worker_channel.destroy()
        self.__response_channel.destroy()
     
    def should_adjustproducer(self):
        return self.__adjustproducer 
                    
            
class WorkerMsgOper(object):
    def __init__(self,  workerid,  responseid, workerlock, channelspace):
        self.__workrid = workerid
        self.__responseid = responseid
        self.__workerlock = workerlock
        self.__channelspace = channelspace
        self.__atomlib = ctypes.cdll.LoadLibrary('/home/pan/workspace/crawler/core/atomoper.so')
        self.__worker_channel = MessageChannel(self.__atomlib, isproducer = False, channelid = workerid, length = self.__channelspace)
        self.__response_channel = MessageChannel(self.__atomlib, isproducer = True, channelid = responseid, length = self.__channelspace)

    def put_response(self, responses):
        """acquire the lock ,do the mutex work, then relase the lock"""
        self.__workerlock.acquire()
        self.__response_channel.put_msgs(responses)
        self.__workerlock.release()
        
    def get_msgs(self, maxcount):
        return self.__worker_channel.get_msgs(maxcount)   
        
    def destroy(self):
        self.__response_channel.destroy()
        self.__worker_channel.destroy()   
        
class MessageChannel(object):   
    LEFTSPACE_OFF = 0
    MSGCNT_OFF = 4
    FRONTSLOT_OFF = 8
    REARSLOT_OFF = 12
    DATAOFFSET_OFF = 16
    
    SLOTS_OFF = 20
    SLOTSCNT = 300
    SLOTSLEN = (300 << 3) #one slot occupies 8 bytes
    
    METALEN = SLOTSLEN +SLOTS_OFF

    def __init__(self, atomlib, isproducer, channelid, length =SERV_WORKER_CHAN_LEN ):
        self.__atomlib = atomlib
        self.__isproducer = isproducer
        self.__length = length
        self.__channelid = channelid
        self.__fd = os.open('/home/pan/tmp/crawler%d' %channelid , os.O_RDWR|os.O_CREAT)
        os.lseek(self.__fd, self.__length + 64, os.SEEK_SET)
        os.write(self.__fd, 'EOF')
        self.__msgqueue = mmap.mmap(self.__fd, self.__length)
        
        self.__msgcnt = 0
        self.__frontslot = 0
        self.__rearslot = 0

        if isproducer:
            self.__msgqueue.seek(0)
            self.__msgqueue.write(struct.pack("iiiii", self.__length - MessageChannel.METALEN , 0,0,0, MessageChannel.METALEN ))
            
     
        
    def get_leftspace(self):
        leftspace = ctypes.c_int.from_buffer(self.__msgqueue, MessageChannel.LEFTSPACE_OFF).value  
        return leftspace 
        
    def get_msgs(self, maxcount):
        """
        consumer: msg layout in mem : leftspace, msgcnt, frontslot, rearslot  dataoffset,[msglen, msgcontent_offset] ...
        """
        """lock"""
        msgs = []
        self.__msgqueue.seek(MessageChannel.FRONTSLOT_OFF)
        """
        leftspace, self.__msgcnt, self.__frontpos =  struct.unpack("iii", self.__msgqueue.read(12))
        print 'hehehhe is %d' %self.__channelid
        print leftspace, self.__frontpos, self.__msgcnt
        return
        """
        self.__frontslot,  = struct.unpack("i", self.__msgqueue.read(4))
        while maxcount:
            cmsg_count = ctypes.c_int.from_buffer(self.__msgqueue, 4) #msgcnt
            print self.__frontslot, cmsg_count
            pointer_msgcnt = ctypes.pointer(cmsg_count)
            self.__msgcnt = self.__atomlib.atom_set(pointer_msgcnt, 0)
            packedmsg = None
            releasespace = 0
            print 'hehe'
            print self.__msgcnt
            #print 'msgcnt is %d front %d'  %self.__msgcnt  %self.__frontpos
            if not self.__msgcnt:
                break
            print 'msgcnt is  %d' %self.__msgcnt
            while self.__msgcnt:
                #print 'frontslot %d' %self.__frontslot
                self.__msgqueue.seek(MessageChannel.SLOTS_OFF + (self.__frontslot << 3))
                msgdata_start, msgdata_len = struct.unpack("ii", self.__msgqueue.read(8))
                print 'msgdata_start is , msgdata_len is  ...', self.__frontslot, msgdata_start, msgdata_len
                print 'len', self.__length
                self.__msgqueue.seek(msgdata_start)
                
                if self.__length - msgdata_start > msgdata_len:
                    print 1
                    packedmsg = self.__msgqueue.read(msgdata_len)
                else:
                    print 2
                    packedmsg = self.__msgqueue.read(self.__length - msgdata_start)
                    self.__msgqueue.seek(MessageChannel.METALEN)
                    packedmsg += self.__msgqueue.read(msgdata_len + msgdata_start - self.__length)
                
                if len(packedmsg) > 50:
                    rstmsg = RequestMessage()
                    _seqno, _websiteno, _typeno, _htmlcontent = rstmsg.unpack(packedmsg)
                    print '_seqno is ', _seqno
                
                self.__frontslot += 1
                if self.__frontslot == MessageChannel.SLOTSCNT:
                    self.__frontslot = 0 
                self.__msgcnt -= 1
                #msgdata_len =(msgdata_len + 3) & ~3  #align
                releasespace += msgdata_len        
                msgs.append(packedmsg)
            leftspace = ctypes.c_int.from_buffer(self.__msgqueue, MessageChannel.LEFTSPACE_OFF)
            pointer_leftspace = ctypes.pointer(leftspace)
            """call c func to do atomic adjustment"""
            ret = self.__atomlib.atom_add(pointer_leftspace ,releasespace)
            print 'ret is %d ' %ret
            """
            if not self.__atomlib.atom_add(pointer_leftspace ,releasespace):
                self.__logging.error("MessageChannel: atom_add in get_msgs")
                #return None
            """
            maxcount -= 1       
        self.__msgqueue.seek(MessageChannel.FRONTSLOT_OFF)
        print self.__frontslot
        self.__msgqueue.write(struct.pack("i", self.__frontslot))
        """release the lock"""
        return msgs
                
  
    def put_msgs(self, msgs):
        """
        producer
        """             
        usespace = 0
        self.__msgqueue.seek(MessageChannel.MSGCNT_OFF)
        self.__msgcnt ,self.__frontslot, self.__rearslot, self.__dataoffset = struct.unpack("iiii", self.__msgqueue.read(16)) 
        for msg in msgs:
            startoffset = self.__dataoffset
            if len(msg) > 50:
                rstmsg = RequestMessage()
                _seqno, _websiteno, _typeno, _htmlcontent = rstmsg.unpack(msg)
                print '_seqno is ', _seqno
            print 'put  msgdata_start is , msgdata_len is  ...  chanel id ',startoffset, len(msg), self.__channelid
            self.__msgqueue.seek(MessageChannel.SLOTS_OFF + (self.__rearslot << 3))
            self.__msgqueue.write(struct.pack("ii",  startoffset, len(msg)))
            self.__rearslot += 1
            if self.__rearslot == MessageChannel.SLOTSCNT:
                self.__rearslot = 0 

            self.__msgqueue.seek(startoffset)
            if self.__length - startoffset > len(msg):
                self.__msgqueue.write(msg)
                self.__dataoffset += len(msg)
            else:
                self.__msgqueue.write(msg[ :self.__length - startoffset ])
                self.__msgqueue.seek(MessageChannel.METALEN)
                self.__msgqueue.write(msg[self.__length - startoffset: ])
                self.__dataoffset += len(msg) -  self.__length + MessageChannel.METALEN
                                                              
            self.__msgcnt += 1    
            usespace += len(msg)   
            #usespace = (usespace + 3) & ~3    
        leftspace = ctypes.c_int.from_buffer(self.__msgqueue, MessageChannel.LEFTSPACE_OFF)
        pointer_leftspace = ctypes.pointer(leftspace)
        ret = self.__atomlib.atom_sub(pointer_leftspace, usespace)
        print 'ret is  f %d' %ret
        """
        if not self.__atomlib.atom_sub(pointer_leftspace, usespace):
            self.__logging.error("MessageChannel: atom_add in get_msgs")
            return None     
        """  
        cmsg_count = ctypes.c_int.from_buffer(self.__msgqueue, 4)
        pointer_msgct = ctypes.pointer(cmsg_count)
        """call c func to do atomic adjustment"""
        ret = self.__atomlib.atom_add(pointer_msgct, len(msgs))
        print 'ret is d %d ' %ret
        """
        if not self.__atomlib.atom_add(pointer_msgct, len(msgs)):
            self.__logging.error("MessageChannel: atom_add in get_msgs")
            return None
        """
        self.__msgqueue.seek(MessageChannel.REARSLOT_OFF)
        self.__msgqueue.write(struct.pack("ii",  self.__rearslot, self.__dataoffset))
        return 0
  
  
    def destroy(self):
        self.__msgqueue.close() 
        self.__fd.close()
        
        
            