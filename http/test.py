import urlsoper 
import crawler.dns.dnsoper as dnsoper
import crawler.net.ioloop as ioloop
import crawler.net.iostream as iostream
import htmlgetter
import functools

def save_ok(result, error):
    print 'save ok %s' %result

def get_ok(result, error):
    print 'get ok '
    print result

def get_htmlcontent(seqno, hostname, lefturl, websiteno, typeno, ipaddr):
    if not ipaddr:
        print hostname
        print 'ipaddr error' 
        return
    html_getter = htmlgetter.HtmlGetterPool(None).get_free_htmlgetter(1)
    html_getter.connect(seqno, hostname, ipaddr, 80, lefturl, websiteno, typeno)

def put_urlsready(urlsinfofromdb):
    print urlsinfofromdb
    seqno = 0
    for urlinfo in urlsinfofromdb:
        url = urlinfo[0]
        websiteno = urlinfo[1]
        typeno = urlinfo[2]
        #we first put this url in the urlinhandling
        retstart = url.find('://')
        if retstart == -1:
            retstart = 0
        retend1 = url.find(':')
        retend2 = url.find('/')
        if retend1 == -1:
           retend = retend2
           if retend2 == -1:
               retend = None
        else:
            retend = retend1
        hostname = url[retstart : retend]
        if retend2 == -1:
            lefturl = '/'
        else:
            lefturl = url[retend2 : ]
        wrap_callback = functools.partial(get_htmlcontent, seqno, hostname, lefturl, websiteno, typeno)
        seqno += 1
        dnsoper_impl.get_ipaddr(hostname, wrap_callback)


        
         

             
             

def handle_cmd(cmd):
    cmd = cmd.strip()
    if cmd == 'start':
        urloper_impl.saveurls(((1, 1,"http://book.sina.com.cn/news/list/31.shtml" ),), save_ok)
    elif cmd == 'get':
        urloper_impl.geturls(5, put_urlsready)
        


if __name__ == '__main__':
    ioloop_impl = ioloop.IOLoop.instance()
    stream = iostream.PipeIOStream(0)
    stream.read_bytes(30, None, handle_cmd)
    urloper_impl = urlsoper.UrlsOper('mongodb')
    dnsoper_impl = dnsoper.DnsOper()
    ioloop_impl.start()
    