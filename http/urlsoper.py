from crawler.sql.dbinfo import urlsqueuedb
import crawler.general.lock as lock
import crawler.sql.mongo as mongo


class UrlsOper(object):
    urlsinsert_mutex =lock.NoLock() # threading.Lock()
    urlspop_mutex = lock.NoLock()#threading.Lock()
    mutex_timeout = 1
    curpopid = 0 #current pop id
    curinsertid = 0 #current insert id
    def __init__(self, dbtype):   
        if dbtype == 'mongodb':
            self._db_impl = mongo.MongoOper("crawler",urlsqueuedb.host,urlsqueuedb.port,urlsqueuedb.dbname,urlsqueuedb.dbuser,urlsqueuedb.dbpass,
                                       maxcached=11,maxconnections=50)
        elif dbtype == 'mysql':
            #self._db_impl =
            pass
        
        
    def saveurls(self, urlsinfo, callback):
        if not UrlsOper.urlsinsert_mutex.acquire(1) :
            return -1
        else:
            idstart = UrlsOper.curinsertid
            UrlsOper.curinsertid += len(urlsinfo)  
            UrlsOper.urlsinsert_mutex.release()
            
            self.__do_saveurls(urlsinfo, idstart, callback)
            return 0
        
    def geturls(self, len, callback):
        if not UrlsOper.urlspop_mutex.acquire(True) :
            return -1
        else:
            idstart = UrlsOper.curpopid
            UrlsOper.curpopid += len  
            UrlsOper.urlspop_mutex.release()
            self.__do_geturls(idstart, len, callback)
            return 0         
         
    def __do_saveurls(self, urlsinfo, idstart, callback):
        self._db_impl.saveurls(urlsinfo, idstart, callback)
        
    def __do_geturls(self, idstart, len, callback):
        self._db_impl.geturls(idstart, len, callback)
        
              
              
              
              
              