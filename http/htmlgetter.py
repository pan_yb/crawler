import socket
import crawler.core.lru as lru
import crawler.general.singleton_manager as singleton_manager
import htmlparser_proxy
import crawler.general.lock as lock
import crawler.net.iostream as iostream
import message

STATE_ERROR, STATE_CONNECTING, STATE_READING, STATE_PARSE = range(4)


class HtmlGetter(object):
    def __init__(self, pool, logging, parser_impl ):
        self.__pool = pool
        self.__logging = logging
        self.__parser_impl = parser_impl
        self.__close_afterwards = False
        
    def connect(self, seqno, hostname, ipaddr, port, url, websiteno, typeno):
        """
        url  the pure url without hostname or ipaddr
        website and typeno is used in parser processing
        seqno is sequence no is used to ack 
        """
        self.__state = STATE_CONNECTING
        self.__seqno = seqno
        self.__hostname = hostname
        self.__ipaddr = ipaddr
        self.__header = {}
        self.__url = url
        self.websiteno = websiteno
        self.__typeno = typeno
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
        self.__stream = iostream.IOStream(s)
        try:
            print 'ipaddr is %s' %ipaddr
            self.__stream.connect((ipaddr, port), callback = self.__connection_ready )
        except IOError:
            self.__logging.warning("connect error")
            raise
        print 'connect'
        
    def connection_ready(self, seqno, url, typeno):
        print 'in hear'
        self.__seqno = seqno
        self.__url = url
        self.__typeno = typeno
        self.__connection_ready()

    def __connection_ready(self):
        request_str = b"GET %s HTTP/1.1\r\nHost: %s\r\nUser-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:7.0.1) \
         Gecko/20100101 Firefox/7.0.1\r\n \
         Accept    text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\n \
         Connection: Keep-Alive\r\n\r\n" % (self.__url, self.__hostname)
        try:
            self.__stream.write(request_str)
            print request_str
            self.__state = STATE_READING
            self.__stream.read_until(b"\r\n\r\n", self.__on_header)
        except IOError:
            self.__error_func()

    def __on_header(self, data):
        print 'in header'
        print data
        lines = data.split(b"\r\n")
        firstline = lines[0]
        httpstate = firstline.split(b' ')[1].strip()
        if httpstate[0] != '2':
            warningmsg = 'HtmlParser: url =  (%s)  failed ' % self.__url
            self.__logging.warning(warningmsg)
            self.close()
            return
        lines = data[1 : ]     
        for line in data.split(b"\r\n"):
            linepair = line.split(b":")
            if len(linepair) == 2:
                print linepair[0].strip(), linepair[1].strip()
                self.__header[linepair[0].strip()] = linepair[1].strip()
        if (not self.__header.has_key(b"Connection")) or self.__header[b"Connection"] == 'close':
            self.__close_afterwards = True
            
        if self.__header.has_key(b"Content-Length"):
            self.__stream.read_bytes(int(self.__header[b"Content-Length"]) , self.__on_body)
        elif self.__header.has_key(b"Transfer-Encoding"):
            self.__allchunkdata =  self.__chunkdata = ''
            self.__lenstart = 0
            self.__stream.read_until(b"\r\n\r\n", self.__handle_chunk)
        else:
            self.__state = STATE_ERROR
            warningmsg = 'HtmlParser: url = (%) failed ' % self.__url
            self.__logging.warning(warningmsg)
            self.__stream.close()
    
    def __handle_chunk(self, allchunkdata):
        self.__allchunkdata += allchunkdata
        lenstart = self.__lenstart
        try:
            while True:
                lenend = self.__allchunkdata.find('\r\n', lenstart)
                chunkstart = lenend + 2
                if lenend == -1 or chunkstart >= len(self.__allchunkdata):
                    self.__stream.read_until(b"\r\n\r\n", self.__handle_chunk)
                    return
                chunklen = int(self.__allchunkdata[lenstart : lenend], 16)
                if chunklen:
                    if chunklen + chunkstart + 2 >= len(self.__allchunkdata):
                        self.__stream.read_until(b"\r\n\r\n", self.__handle_chunk)
                        return 
                    chunkdata = self.__allchunkdata[chunkstart : chunkstart + chunklen]
                    self.__chunkdata += chunkdata
                    lenstart =  chunkstart + chunklen + 2   
                    self.__lenstart = lenstart  
                else:
                    self.__on_body(self.__chunkdata)
                    return 
        except IOError:
            self.__error_func()
               
     
    def __on_body(self, data):
        print 'on_body'
        msg = message.RequestMessage(self.__seqno , self.websiteno, self.__typeno, data)
        self.__parser_impl.parser(msg.pack())
        if self.__close_afterwards:
            self.close()
        else:
            self.release()
    
    def release(self):
        self.__pool.free_htmlgetter(self)
        
    def close(self):
        self.__stream.close()
    
    def get_state(self):
        return self.__state
           
    def __error_func(self):
        warningmsg = 'HtmlParser: url = (%) failed ' % self.__url
        self.__logging.warning(warningmsg)
    	

"""
only long http connection makes sense.
the same websiteno means same website,try to
share same socket channel
"""

class HtmlGetterPool(object):
    def __new__(cls, *args, **kw):
        if not hasattr(cls, '_instance'):
            orig = super(HtmlGetterPool, cls)
            cls._instance = orig.__new__(cls, *args, **kw)
            cls._instance.__initialize(*args, **kw)
        return cls._instance
    
    def __init__(self, logging):
        pass
    
    def __initialize(self, logging):
        self.__logging = logging
        self.__htmlgetters = lru.Lru(5)
        #htmlparser_proxy  is a singleton
        self.__htmlparserfactory = HtmlParserFactory_Server(self.__logging)
        

    
    
    def get_free_htmlgetter(self, websiteno):
        if self.__htmlgetters.has_key(websiteno):
            if len(self.__htmlgetters[websiteno]):
                return self.__htmlgetters.getitem(websiteno).pop(0)       
        else:
            self.__htmlgetters[websiteno] = []
        htmlparser = self.__htmlparserfactory.get_htmlparser(websiteno)   
        htmlgetter = HtmlGetter(self, self.__logging, htmlparser)
        return htmlgetter
                    
    def free_htmlgetter(self, htmlgetter):
        try:
            self.__htmlgetters[htmlgetter.websiteno].append(htmlgetter)
        except KeyError:
            htmlgetter.close()
        
        
        
    def __del__(self):
        for htmlgetter in self.__htmlgetters:
            htmlgetter.close()


"""
return the singleton parser according to the websiteno,
"""
class HtmlParserFactory(object):
 
    def __init__(self):
        pass
    
    """return the corresponding parser
    according to the websiteno"""
    def get_htmlparser(self, websiteno):
        raise NotImplementedError()


"""
return the singleton parser according to the websiteno,
"""
class HtmlParserFactory_Server(HtmlParserFactory):
    def __new__(cls, *args, **kw):
        if not hasattr(cls, '_instance'):
            orig = super(HtmlParserFactory_Server, cls)
            cls._instance = orig.__new__(cls, *args, **kw)
            cls._instance.__initialize(*args, **kw)
        return cls._instance
 
    def __init__(self, logging):
        pass
    
    def __initialize(self, logging):
        self.__htmlparser_proxy =  htmlparser_proxy.HtmlParser_Proxy(logging, lock.NoLock())
          
    
    """return the corresponding parser
    according to the websiteno"""
    def get_htmlparser(self, websiteno):
        return self.__htmlparser_proxy
        #return htmlparser_proxy.HtmlParser_Test()
            
            
"""
return the singleton parser according to the websiteno,
 the parser should be registered in the pool,
 if the parser is in the pool, return it, if not,
new one ,and register it in the pool
"""
class HtmlParserFactory_Woker(HtmlParserFactory):
    def __init__(self):
        self.__maxparsercount = 50
        self.__singleton_manager = singleton_manager.SingletonManager(self.__maxparsercount)
        self.__strategymap = strategy_map.allstrategy_info

    
    def new_htmlparser(self, websiteno):
        return self.__strategymap[websiteno]()
        
        
    
    """return the corresponding parser
    according to the websiteno"""
    def get_htmlparser(self, websiteno):
        try :
            parser = self.__singleton_manager.find(websiteno)
        except ValueError, key:
            try:
                parser = new_htmlparser(websiteno)
            except ValueError, key:
                """logging@@@@@@@@@@@@@@@@@@@@@@@@@@@"""
                pass
                return None
        return parser
            
                  
        

        
