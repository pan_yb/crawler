"""
this class is a singleton class, used by all htmlgetters,
when they get the htmls , put the htmls to the workers to 
handle
"""
import crawler.http.message as message

class HtmlParser_Proxy(object):
    def __new__(cls, *args, **kw):
        if not hasattr(cls, '_instance'):
            orig = super(HtmlParser_Proxy, cls)
            cls._instance = orig.__new__(cls, *args, **kw)
            cls._instance.__initialize(*args, **kw)
        return cls._instance
    
    def __init__(self, logging, lock_impl):
        pass
    
    def __initialize(self, logging, lock_impl):
        self.__connections = []
        self.__servermsgoper = message.ServerMsgOper(logging)
        self.__lock_impl = lock_impl
        
    """
    because this class is singleton class, we need a lock to protect 
    the share data among threads
    """
    def parser(self, packedmsg):
        self.__lock_impl.acquire()
        self.__servermsgoper.put_msg_to_worker(packedmsg)
        self.__lock_impl.release()
        
    def get_responses(self):
        """
        responsemsgs = [response1, response2...]  len<= 1024
        """
        self.__lock_impl.acquire()
        responsemsgs = self.__servermsgoper.get_responses()
        self.__lock_impl.release()
        return responsemsgs
        
        
    def should_adjustproducer(self):
        return self.__servermsgoper.should_adjustproducer()   


class HtmlParser_Test(object):
    def __new__(cls, *args, **kw):
        if not hasattr(cls, '_instance'):
            orig = super(HtmlParser_Test, cls)
            cls._instance = orig.__new__(cls, *args, **kw)
            cls._instance.__initialize()
        return cls._instance
    
    def __init__(self):
        pass
    
    def __initialize(self):
        pass
        
    """
    because this class is singleton class, we need a lock to protect 
    the share data among threads
    """
    def parser(self, packedmsg):
        rqmsg = message.RequestMessage()
        print rqmsg.unpack(packedmsg)[3]

        
    def get_responses(self):
        pass
        