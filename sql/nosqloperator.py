import urlsoper
import crawler.asyncmongo
import crawler.exception.sqlerror
class Nosqloperator(urlsoper):
    
    def __init__(self,poolid,host,port,dbname, dbuser, dbpass, maxcached=10,maxconnections=50):
        urlsoper.__init__()
        self._db = asyncmongo.Client(pool_id = poolid,host = host, port = port ,maxcached = maxcached,maxconnections =maxconnections,dbname = dbname,
                                     dbuser = dbuser, dbpass = dbpass); 


    def geturls(self, idstart, len, callback):
        self._db.find({'id':{'$gt':idstart}}.sort({'id':1}).limit(len), callback = callback)
    
    def saveurls(self, urls, id, callback):
        for url in urls:
            self._db.insert({'id':id,'url':url}, callback = callback)
            id += 1
        
"""       
    def _on_geturls_response(self,response,error):
        if error:
            raise  Sqlerror,'mongodb geturls error happened!' 
        
        
    def _on_saveurls_response(self,response,error):
        if error:
            raise  Sqlerror,'mongodb seturls error happened!'   
"""