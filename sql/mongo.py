import crawler.asyncmongo as asyncmongo
import crawler.general.sqlerror
#import crawler.exception.sqlerror
class MongoOper(object):#urlsoper):   
    def __init__(self,poolid,host,port,dbname,dbuser, dbpass, maxcached=10,maxconnections=50):
        #urlsoper.__init__()
        self.__observer_get = self.__observer_save = None
        self.__connectionname = 'url'
        self.__db = asyncmongo.Client(pool_id = poolid,host = host, port = port ,maxcached = maxcached,maxconnections =maxconnections,dbname = dbname, 
                                      dbuser = dbuser , dbpass = dbpass) 
        self.__cursor = self.__db.connection(self.__connectionname)

    def geturls(self, idstart, len, observer_get):
        self.__observer_get = observer_get
        if observer_get ==  None:
            callback = None
        else:
            callback = self.__on_geturls_response            
        self.__cursor.find({'id':{'$gte':idstart, '$lt':idstart+len}}, sort = [('id',1)], callback = callback)
        
    def saveurls(self, urlsinfo, id, observer_save):
        """
        urlinfo =( websiteno, typeno, url)
        """
        self.__observer_save = observer_save
        if observer_save == None:
            callback = None
        else:
            callback = self.__on_saveurls_response
        docs = []
        print 'len of urlsinfo is %d', len(urlsinfo)
        for urlinfo in urlsinfo:
            docs.append({'id':id,'url':urlinfo[2],'websiteno': urlinfo[0],'typeno': urlinfo[1]})
            id += 1
        self.__cursor.insert(docs, callback = callback)


        
      
    def __on_geturls_response(self, urlsinfo, error):
        if not self.__observer_get:
            return
        if error:
            self.__observer_get(None)
            return
        wrapdata = self.__wrapdata(urlsinfo)
        self.__observer_get(wrapdata)
    
    def __wrapdata(self, urlsinfo):
        wrapdata = []
        for urlinfo in urlsinfo:
            url = urlinfo['url'].encode('ascii')
            websiteno = urlinfo['websiteno']
            typeno = urlinfo['typeno']   
            wrapdata.append((url, websiteno, typeno))
        return wrapdata
      
    def __on_saveurls_response(self,response,error):
        if error:
            raise  sqlerror.Sqlerror,'mongodb seturls error happened!'   
