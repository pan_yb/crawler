#include <stdio.h>
int atom_add(int* pdest, int delta)
{
	int oldval = *pdest;
	int newval;
	int trynum = 5;
	int i=0;
	do
	{
		if (i++ == trynum)
			return -1;
		oldval = *pdest;
		newval = oldval + delta ;
		//printf("add_atomic oldval is %d,  newval is%d\n", oldval, newval);
	}while(!__sync_bool_compare_and_swap(pdest, oldval, newval));
	return 0;
}


int atom_sub(int* pdest, int delta)
{
	int oldval = *pdest;
	int newval;
	int trynum = 5;
	int i=0;
	do
	{
		if (i++ == trynum)
			return -1;
		oldval = *pdest;
		newval = oldval - delta ;
		//printf("sub_atomic oldval is  %d  newval is %d\n", oldval, newval);
	}while(!__sync_bool_compare_and_swap(pdest, oldval, newval));
	return 0;
}


int atom_set(int* pdest, int value)
{
	return __sync_lock_test_and_set(pdest, value);	
}


