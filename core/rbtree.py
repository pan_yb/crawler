RED , BLACK = range(2)

class RBNode(object):
    def __init__(self, key, value , color, parent):
        self.key = key
        self.value = value
        self.color = color
        self.left = None
        self.right = None
        self.parent = parent
        
        
class RBTree(object):
    def __init__(self):
        self.root = None
        
    def insert(self, key, value):
        if self.root  == None:
            self.root = insernode = RBNode(key, value, BLACK, None)
            return
        curnode = self.root
        while curnode:
            prenode = curnode
            if key < curnode.key :
                curnode = curnode.left
            elif key > curnode.key:
                curnode = curnode.right
            else :
                raise RBerror('insert error, already exists')
            
        if prenode.key < key:
            prenode.right = insertnode = RBNode(key,value, RED, prenode)
        else :
            prenode.left = insertnode =  RBNode(key, value, RED, prenode)
        self.fix_insert(insertnode)
        return  insertnode
        
            
            
    def __left_rotate(self , rbnode):
        oriright = rbnode.right
        rbnode.right = oriright.left
        oriright.left.parent = rbnode
        oriright.parent = rbnode.parent
        #root
        if not rbnode.parent: 
            self.root = oriright
        elif rbnode == rbnode.parent.left:
            rbnode.parent.left = oriright
        else :
            rbnode.parent.right = oriright
        oriright.left = rbnode
        rbnode.parent = oriright 
                

        
        
    def __right_rotate(self, rbnode):
        orileft = rbnode.right
        rbnode.left = orileft.right
        orileft.right.parent = rbnode
        orileft.parent = rbnode.parent
        #root
        if not rbnode.parent: 
            self.root = orileft
        elif rbnode == rbnode.parent.left:
            rbnode.parent.left = orileft
        else :
            rbnode.parent.right = orileft
        orileft.right = rbnode
        rbnode.parent = orileft 
                   
        
    def fix_insert(self, rbnode):
        prbnode = rbnode.parent
        while prbnode and prbnode.color == RED:
            grbnode = prbnode.parent
            if prbnode == grbnode.left:
                anclenode = grbnode.right
                if anclenode.color == RED:
                    prbnode.color = BLACK
                    anclenode.color = BLACK
                    grbnode.color = RED
                    rbnode = grbnode
                    prbnode = rbnode.parent
                    continue
                #case 2
                elif rbnode == prbnode.right:
                    self.__left_rotate(prbnode)                    
                    rbnode, prbnode = prbnode, rbnode
                    #case 3
                    prbnode.color = BLACK
                    grbnode.color = RED  
                    self.__right_rotate(grbnode)     
            else:
                anclenode = grbnode.left
                if anclenode.color == RED:
                    prbnode.color = BLACK
                    anclenode.color = BLACK
                    grbnode.color = RED
                    rbnode = grbnode
                    continue
                elif rbnode == prbnode.left:
                    self.__right_rotate(prbnode)                    
                    rbnode, prbnode = prbnode, rbnode
                    #case 3
                    prbnode.color = BLACK
                    grbnode.color = RED
                    self.__left_rotate(grbnode)     

                              
                
                
    def fix_delete(self, curnode, isleft):
        while self.root != curnode and curnode.color == BLACK:   
            parent = curnode.parent
            if isleft:          
                brother = parent.right
                if brother.color == RED:
                    brother.color = BLACK
                    parent.color = RED
                    self.__left_rotate(parent)
                    continue
                else:   
                    if brother.left.color == BLACK and brother.right.color == BLACK:
                        brother.color = RED
                        curnode = parent
                        continue
                    elif brother.left.color == RED and brother.right.color == BLACK:
                        brother.color = RED
                        brother.left.color = BLACK
                        self.__right_rotate(brother)
                        continue
                    else:
                        brother.color = parent.color
                        parent.color = BLACK
                        brother.right.color = BLACK
                        self.__left_rotate(pather)
                        curnode = root
                        
            else :
                brother = parent.left
                if brother.color == RED:
                    brother.color = BLACK
                    parent.color = RED
                    self.__right_rotate(parent)
                    continue
                else:   
                    if brother.right.color == BLACK and brother.left.color == BLACK:
                        brother.color = RED
                        curnode = parent
                        continue
                    elif brother.right.color == RED and brother.left.color == BLACK:
                        brother.color = RED
                        brother.right.color = BLACK
                        self.__left_rotate(brother)
                    else :
                        brother.color = parent.color
                        parent.color = BLACK
                        parent.left.color = BLACK
                        self.__right_rotate(parent)
                        curnode = root                     
        curnode.color = BLACK       
                    
                    
                    
    def find_node(self, key):
        curnode = self.root
        while curnode:
            if key == curnode.key:
                return curnode
            elif key < curnode.key:
                curnode = curnode.left
            else:
                curnode = curnode.right
        return curnode
     
    def find_value(self, key):
        curnode = self.find_node(key)
        if curnode: 
            return curnode.value     
        return None      
                
                
    def delete_node(self, key):
        curnode = self.root
        while curnode :
            if key < curnode.key:
                curnode = curnode.left
            elif key > curnode.key:
                curnode = curnode.right
            else:
                break;
        if not curnode:
            return
        parent = curnode.parent
        if not curnode.left:
            childnode = curnode.right
        elif not curnode.right:
            childnode = curnode.left
        else:
            rightleast = curnode.right
            while rightleast.left:
                rightleast = rightleast.left
            childnode = rightleast
        if childnode:
            childnode.parent = parent  
            childnode.left = curnode.left
            childnode.right = curnode.right  
        if curnode == parent.left:
            if parent: 
                parent.left = childnode
            else : #root
                self.root = childnode
            curnode.parent = curnode.left = curnode.right = curnode.key = curnode.right = None
            fix_delete(childnode,  True)
        else :
            if parent:
                parent.right = childnode
            else : #root
                self.root = childnode
            curnode.parent = curnode.left = curnode.right = curnode.key = curnode.right = None
            fix_delete(childnode,  False)
        
        
            
        
                     