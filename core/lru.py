class LruNode(object):
    def __init__(self, prenode, nextnode, key, value):
        self.prenode = prenode
        self.key = key
        self.value = value
        self.nextnode = nextnode
        
    def __del__(self):
        del value    
        del key

class Lru(object):
    def __init__(self, maxcount):
        self.__data = {}
        self.__listhead = LruNode(None, None, None, None) 
        self.__listend = LruNode(self.__listhead, None, None, None) 
        self.__listhead.nextnode = self.__listend
        self.__count = 0
        self.__maxcount = maxcount
     
    def has_key(self, key):
        return self.__data.has_key(key)
     
        
    def __setitem__(self, key, value):
        if self.__data.has_key(key):
            lrunode = self.__data[key]
            lrunode.value = value
            if self.__count == 1:
                return 
            lrunode.prenode.nextnode = lrunode.nextnode
            lrunode.nextnode.prenode = lrunode.prenode
        else:           
            lrunode = LruNode(None, None, key, value)
            self.__count += 1
            if self.__count == self.__maxcount:
                delnode = self.__listend.prenode
                delnode.prenode.nextnode = self.__listend
                self.__listend.prenode = delnode.prenode
                delnode.prenode = delnode.nextnode = None
                print key, delnode.key ,'fdsfsdffffffffffffffff'
                print self.__data.keys(), self.__count, self.__maxcount
                del self.__data[delnode.key]
               # del self.__listend.nextnode
        lrunode.nextnode = self.__listhead.nextnode
        lrunode.nextnode.prenode = lrunode
        lrunode.prenode = self.__listhead
        lrunode.prenode.nextnode = lrunode
        self.__data[key] = lrunode

    def __getitem__(self, key):
        lrunode = self.__data[key]
        return lrunode.value            


    def getitem(self, key):
        lrunode = self.__data[key]
        lrunode.prenode.nextnode = lrunode.nextnode            
        lrunode.nextnode.prenode = lrunode.prenode

        lrunode.nextnode = self.__listhead.nextnode
        lrunode.nextnode.prenode = lrunode
        lrunode.prenode = self.__listhead
        lrunode.prenode.nextnode = lrunode
        return lrunode.value            
        
    def __del__(self):
        self.clear()
            
        
    def clear(self):
        for i in self.__data:
            lrunode = self.__data[i]
            lrunode.prenode = lrunode.nextnode = None
            del self.__data[i]

            
            
        